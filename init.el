;; show unbalanced paranthesis
(show-paren-mode t)
(setq show-paren-style 'expression)
(when (eq major-mode 'emacs-lisp-mode)
  (add-hook 'after-save-hook 'check-parens t))


;; Turn off mouse interface
(when window-system
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1))

;; Skip startup screen
(setq inhibit-startup-screen t)
(setq initial-scratch-message "")

;; Initialize package system
(require 'package)
(setq package-archives
      '(("org"       . "https://orgmode.org/elpa/")
        ("melpa-stable" . "https://stable.melpa.org/packages/")
        ("melpa" . "https://melpa.org/packages/")
        ("gnu" . "https://elpa.gnu.org/packages/")))
(package-initialize)

;; Now install use-package.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))

;; turn on auto compile for elisp code
;; https://github.com/emacscollective/auto-compile
(setq load-prefer-newer t)
(use-package auto-compile
  :ensure t
  :config
(auto-compile-on-load-mode)
(auto-compile-on-save-mode)
(setq auto-compile-display-buffer nil)
(setq auto-compile-mode-line-counter t))

(when (memq window-system '(mac ns))
    (setq ns-use-srgb-colorspace nil))

;; Keeps variables and faces added by customise-*
;; commands in separate file. These files shouldn't be
;; modified by hand
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

;; https://github.com/purcell/exec-path-from-shell
;; sets up PATH, MANPATH, exec-path from shell (bash/zsh)
(use-package exec-path-from-shell :ensure t)
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

;; enable smart paranthesis
(use-package smartparens :ensure t)
(add-hook 'emacs-lisp-mode-hook #'smartparens-mode)

;; Finally load packages and config.org
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(org-babel-load-file (concat user-emacs-directory "config.org"))

;; auto-completion framework
(use-package company :ensure t)                                   ; load company mode
(company-mode)
(setq company-idle-delay .3)                         ; decrease delay before autocompletion popup shows
(setq company-echo-delay 0)                          ; remove annoying blinking
(setq company-begin-commands '(self-insert-command)) ; start autocompletion only after typing
(add-hook 'after-init-hook 'global-company-mode)

(put 'erase-buffer 'disabled nil)
(global-hl-line-mode)
(setq org-src-fontify-natively t)
(put 'upcase-region 'disabled nil)
