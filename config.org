* Pre Package
#+BEGIN_SRC emacs-lisp

  (menu-bar-mode -1)
  ;;(toggle-scroll-bar -1)
  (tool-bar-mode -1)
  ;;(scrollnnnnnnnnnnnnn-bar-mode:qd)
  (setq visible-bell 1)
  (global-font-lock-mode t)

  (setq-default cursor-type 'bar)

  ;; set font to 13pt
  (set-face-attribute 'default nil :height 130)

  (setq mac-allow-anti-aliasing t)

  ;; Keep all backup and auto-save files in one directory
  (setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
  (setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))

  ;; Don't you dare to warn me!
  (setq large-file-warning-threshold nil)

  ;; Allow pasting selection outside of Emacs
  (setq x-select-enable-clipboard t)

  ;; Show keystrokes in progress since the default is to now show the keys
  ;; pressed in quick succession
  (setq echo-keystrokes 0.1)

  ;; Move files to trash when deleting
  (setq delete-by-moving-to-trash t)

  ;; Transparently open compressed files
  (auto-compression-mode t)

  ;; Answering just 'y' or 'n' will do
  (defalias 'yes-or-no-p 'y-or-n-p)

  ;; Remove text in active region if inserting text
  (delete-selection-mode 1)

  ;; Always display line and column numbers in the mode line
  (setq line-number-mode t)
  (setq column-number-mode t)

  ;; Lines should be 85 characters wide, not 72
  (setq-default fill-column 85)

  ;; Never insert tabs
  (set-default 'indent-tabs-mode nil)

  ;; Easily navigate sillycased words
  (global-subword-mode 1)

  ;; Reduce the frequency of garbage collection by making it happen on
  ;; each 25MB of allocated data (the default is on every 0.76MB)
  (setq gc-cons-threshold 25000000)
  (setq gc-cons-percentage 0.6)

  ;; Turn off the blinking cursor
  (blink-cursor-mode -1)

  ;; Enable show parenthesis
  (show-paren-mode t)

  ;; Just run my code.
  (setq org-confirm-babel-evaluate nil)

  ;; turn on word wrap and actions on screen lines instead of logical lines
  ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Visual-Line-Mode.html
  (global-visual-line-mode t)

  (setq show-trailing-whitespace t)

  ;;keep cursor at same position when scrolling
  (setq scroll-preserve-screen-position t)

  ;; scroll one line at a time
  (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
  (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  (setq scroll-step 1) ;; keyboard scroll one line at a time
  (setq scroll-conservatively 10000)
  (setq scroll-margin 3)

  (setq tab-always-indent 'complete)

  ;; delete trailing whitespace in all modes
  (add-hook 'before-save-hook #'delete-trailing-whitespace)

  ;; Snag the user's PATH and GOPATH
  ;; save 3 secs during startup, can we set these during respective modes?
  ;;    (when (memq window-system '(mac ns)) (exec-path-from-shell-copy-env "GOPATH")
  ;;      (exec-path-from-shell-copy-env "PATH"))

  ;; Use a hook so the message doesn't get clobbered by other messages.
  ;; establishes a baseline of startup times.
  (add-hook 'emacs-startup-hook
            (lambda ()
              (message "Emacs ready in %s with %d garbage collections."
                       (format "%.2f seconds"
                               (float-time
                                (time-subtract after-init-time before-init-time)))
                       gcs-done)))

  ;; automatically adds matching closing pair bracket
  (electric-pair-mode 1)


  ;; Set my details
  (setq calendar-latitude 37.4)
  (setq calendar-longitude -121.9)
  (setq calendar-location-name "San Jose, CA")

  (setq user-full-name "Krishnanand Thommandra"
        user-mail-address "devtkrishna@gmail.com")

  (org-toggle-pretty-entities)
#+END_SRC

* Theme and mode-line
#+BEGIN_SRC emacs-lisp

  ;; https://github.com/hlissner/emacs-doom-themes

  (use-package doom-themes
    :ensure t
    :config
    ;; Global settings (defaults)
    (setq doom-themes-enable-bold t)    ; if nil, bold is universally disabled
    (setq doom-themes-enable-italic t) ; if nil, italics is universally disabled
    (load-theme 'doom-one t)

    ;; Enable flashing mode-line on errors
    (doom-themes-visual-bell-config)

    ;; Enable custom neotree theme (all-the-icons must be installed!)
    ;; (doom-themes-neotree-config)
    ;; or for treemacs users
    (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
    (doom-themes-treemacs-config)

    ;; Corrects (and improves) org-mode's native fontification.
    (doom-themes-org-config))

  ;; time display configuration
  (setq display-time-default-load-average nil)
  (setq display-time-day-and-date 1)
  ;;(setq display-time-format "%a %b %d %R%p”)

  ;; https://seagle0128.github.io/doom-modeline/
  ;; https://github.com/jonathanchu/dotemacs/blob/master/doom.el
  (use-package doom-modeline
    :ensure t
    :config
    (progn
      (setq doom-modeline-buffer-file-name-style 'buffer-name)
      (setq doom-modeline-major-mode-icon nil)
      (setq doom-modeline-minor-modes nil)
      (setq doom-modeline-modal-icon nil)
      (setq doom-modeline-buffer-encoding nil)
      (setq doom-modeline-lsp nil)
      (doom-modeline-def-segment utc-time
      "UTC time string of now."
      (propertize (format-time-string " %a %b %d %R%p " nil t) 'face 'mode-line-inactive))
      (doom-modeline-def-modeline 'main
  '(bar workspace-name window-number modals matches buffer-info remote-host buffer-position word-count parrot selection-info)
  '(utc-time misc-info objed-state persp-name battery grip irc mu4e github debug lsp minor-modes input-method indent-info buffer-encoding major-mode process vcs checker))
       (display-time-mode)
      )
    :hook (after-init . doom-modeline-mode))
#+END_SRC

* Packages
** Install following software
brew install aspell
** Third-party packages
#+BEGIN_SRC emacs-lisp

  (use-package which-key
    :ensure t
    :init (which-key-mode)
    :config (which-key-setup-side-window-bottom)
    (setq which-key-idle-delay 0.05))

 (which-function-mode)
(use-package key-chord
    :ensure t
    :config
    (key-chord-mode 1)
    )

  (use-package imenu-list :ensure t)
  (use-package all-the-icons :defer t)

  (use-package hydra
    :ensure t)

  (use-package emojify
    :defer t
    :config
    (progn
      (add-hook 'after-init-hook #'global-emojify-mode)
      (use-package company-emoji :defer t)
      (add-to-list 'company-backends 'company-emoji)))

  ;; add abbreviations into .abbrev_defs as follows
  ;;
  ;; ("owrk" "work" nil 0)
  ;; above causes "owrk" to be fixed to "work" automatically
  ;;
  (progn
    (setq abbrev-file-name (expand-file-name "spell_abbrevs" "~/Dropbox/emacs/"))
    (if (file-exists-p abbrev-file-name)
        (quietly-read-abbrev-file))
    (setq ispell-program-name "aspell")
    (setq ispell-dictionary "en_US")
    (setq flyspell-abbrev-p t)
    (setq flyspell-issue-message-flag nil)
    (setq flyspell-issue-welcome-flag nil)
    (use-package helm-flyspell :ensure t
      :config
      (custom-set-faces '(flyspell-incorrect ((t (:foreground "orange"
                                                              :inverse-video t
                                                              :slant italic
                                                              :weight bold
                                                              :height 1.2)))))
      (setq ispell-silently-savep t)
      (setq ispell-personal-dictionary "~/.emacs.d/.aspell.en.pws")
      :hook (
             (prog-mode . flyspell-prog-mode)
             (text-mode . flyspell-mode)))
    (dolist (hook '(text-mode-hook prog-mode-hook))
      (add-hook hook (lambda () (abbrev-mode 1))))
    )

  (use-package helm
    :ensure t
    :diminish helm-mode
    :init (progn
            (require 'helm-config)
            (use-package helm-projectile
              :ensure t
              :commands helm-projectile
              ;;            :bind ("C-c p h" . helm-projectile)
              )
            (use-package helm-ag :ensure t)
            (setq helm-locate-command "mdfind -interpret -name %s %s"
                  helm-ff-newfile-prompt-p nil
                  helm-M-x-fuzzy-match t)
            (helm-mode)
            (use-package helm-swoop
              :ensure t
              :bind ("C-c h w" . helm-swoop)))
    :bind (("C-c h" . helm-command-prefix)
           ("C-x b" . helm-mini)
           ("C-`" . helm-resume)
           ("M-x" . helm-M-x)
           ("C-x C-f" . helm-find-files)))

  (use-package magit
    :ensure t
    :bind ("C-c g" . magit-status)
    :config
    (setq magit-diff-refine-hunk t)
    (define-key magit-status-mode-map (kbd "q") 'magit-quit-sessiont)
    (defun my-push-to-gerrit ()
     (interactive)
      (magit-git-command-topdir "git push review"))
    (transient-append-suffix 'magit-push "m"
     '("g" "Push to gerrit" my-push-to-gerrit))
    ;; untracked files are disaplayed at the end of the status buffer
    (setq magit-status-sections-hook '(magit-insert-status-headers magit-insert-merge-log magit-insert-rebase-sequence magit-insert-am-sequence magit-insert-sequencer-sequence magit-insert-bisect-output magit-insert-bisect-rest magit-insert-bisect-log magit-insert-unstaged-changes magit-insert-staged-changes magit-insert-stashes magit-insert-unpulled-from-upstream magit-insert-unpulled-from-pushremote magit-insert-unpushed-to-upstream-or-recent magit-insert-untracked-files)))

  (use-package git-timemachine :ensure t)

  (use-package flycheck
    :ensure t
    :init (global-flycheck-mode))

  (use-package helm-flycheck
    :ensure t
    :config
    (define-key flycheck-mode-map (kbd "C-c ! h") 'helm-flycheck)
    (key-chord-define-global "QF"
                             (defhydra flycheck-hydra ()
                               "errors"
                               ("n" flycheck-next-error "next")
                               ("p" flycheck-previous-error "previous")
                               ("h" helm-flycheck "helm" :color blue)
                               ("q" nil "quit"))))


  (use-package undo-tree
    :ensure t
    :config
    (global-undo-tree-mode 1)
    ;; Each node in the undo tree should have a timestamp.
    (setq undo-tree-visualizer-timestamps t)
    ;; Show a diff window displaying changes between undo nodes.
    (setq undo-tree-visualizer-diff t)
    )

  ;; evil mode
  (progn
    (defun air--config-evil ()
      "Configure evil mode."

      ;; Use Emacs state in these additional modes.
      (dolist (mode '(kubernetes-mode
                      dired-mode
                      eshell-mode
                      flycheck-error-list-mode
                      git-rebase-mode
                      org-capture-mode
                      term-mode))
        (add-to-list 'evil-emacs-state-modes mode))

      (delete 'term-mode evil-insert-state-modes)
      (delete 'eshell-mode evil-insert-state-modes)

      ;; Use insert state in these additional modes.
      (dolist (mode '(magit-log-edit-mode))
        (add-to-list 'evil-insert-state-modes mode))

      )

    ;; https://blog.aaronbieber.com/2016/01/23/living-in-evil.html
    (use-package evil
      :ensure t
      :config
      (add-hook 'evil-mode-hook 'air--config-evil)

      (use-package evil-leader
        :ensure t
        :config
        (global-evil-leader-mode))

      (use-package evil-surround
        :ensure t
        :config
        (global-evil-surround-mode))

      (use-package evil-indent-textobject
        :ensure t)

      ;; avoid moving the cursor one position backward
      ;; when exiting insert mode
      (setq evil-move-cursor-back nil)

      (evil-mode 1)
      ))

    ;; Golang support
    (progn
      (use-package go-mode :commands go-mode :ensure t)
      (use-package go-eldoc :ensure t)
      (use-package company-go :ensure t)
      ;;(use-package go-guru :ensure t :ensure-system-package (guru . "go get -u golang.org/x/tools/cmd/guru"))
      (use-package go-guru :ensure t)
      (use-package go-rename :ensure t)
      ;; TODO: setup gorename
      (setq go-rename-command "~/go/bin/gorename")
      (use-package go-projectile :ensure t)
      (use-package helm-go-package :ensure t)
      (defun go-mode-setup ()
        (go-eldoc-setup)
        (add-hook 'before-save-hook 'gofmt-before-save)
        ;; TODO: setup goimports
        (setq gofmt-command "~/go/bin/goimports")
        (local-set-key (kbd "M-.") 'godef-jump)
        (local-set-key (kbd "M-*") 'pop-tag-mark)

        ;; Snag the user's PATH and GOPATH
        (when (memq window-system '(mac ns))
          (exec-path-from-shell-copy-env "GOPATH")
          (exec-path-from-shell-copy-env "PATH"))

        (set (make-local-variable 'compile-command)
             "go build -v && go test -v && go vet")
        (local-set-key (kbd "C-c C-b") 'compile)

        ;; guru settings
        (go-guru-hl-identifier-mode)                    ; highlight identifiers

        (go-eldoc-setup)

        (set-face-attribute 'eldoc-highlight-function-argument nil
                            :underline t :foreground "green"
                            :weight 'bold)

        (unless (file-directory-p "~/.emacs.d/snippets/go-mode")
          (shell-command-to-string "mkdir -p ~/temp/go-snippets")
          (shell-command-to-string "git clone https://github.com/toumorokoshi/go-snippets.git ~/temp/go-snippets")
          (shell-command-to-string "mv ~/temp/go-snippets/snippets/go-mode ~/.emacs.d/snippets/"))

        (use-package go-snippets :ensure t)

        (use-package flycheck-gometalinter
          :ensure t
          :config
          (progn
            (flycheck-gometalinter-setup)))

        ;; skips 'vendor' directories and sets GO15VENDOREXPERIMENT=1
        (setq flycheck-gometalinter-vendor t)
        ;; only show errors
        (setq flycheck-gometalinter-errors-only t)
        ;; only run fast linters
        (setq flycheck-gometalinter-fast t)
        ;; use in tests files
        (setq flycheck-gometalinter-test t)
        ;; disable linters
        (setq flycheck-gometalinter-disable-linters '("gotype" "gocyclo"))
        ;; Only enable selected linters
        (setq flycheck-gometalinter-disable-all t)
        (setq flycheck-gometalinter-enable-linters '("golint"))
        ;; Set different deadline (default: 5s)
        (setq flycheck-gometalinter-deadline "10s")

        ;; speedbar for golang
        (speedbar-add-supported-extension ".go")
        (setq imenu-generic-expression
              '(("type" "^type *\\([^ \t\n\r\f]*\\)" 1)
                ("func" "^func *\\(.*\\) {" 1)))
        (imenu-add-to-menubar "Index")
        ;; Outline mode
        (make-local-variable 'outline-regexp)
        (setq outline-regexp "//\\.\\|//[^\r\n\f][^\r\n\f]\\|pack\\|func\\|impo\\|cons\\|var.\\|type\\|\t\t*....")
        (outline-minor-mode 1)
        (local-set-key "\M-a" 'outline-previous-visible-heading)
        (local-set-key "\M-e" 'outline-next-visible-heading)

        (defun tka-godoc-search ()
          "Retrieves packages matching helm-pattern from godoc.org"
          (let ((fixedArgs '("-s" "-H" "Accept: text/plain"))
                (urlArg (list (url-encode-url (concat "https://godoc.org/?q=" helm-pattern)))))
            (apply 'start-process "curl" nil "curl" (append fixedArgs urlArg))))

        (defun tka-show-GoDoc (candidate)
          (let ((pkg (car (split-string candidate " " t)))
                (docBuffer (get-buffer-create "* GoDoc *")))
            (switch-to-buffer-other-window docBuffer)
            (erase-buffer)
            (call-process "curl" nil docBuffer t "-s" "-H" "Accept: text/plain"
                          (format "https://godoc.org/%s" pkg)))
          (goto-char (point-min)))

        (defun tka-gopkg-list ()
          "Shows Go packages available on localhost and in godoc.org website"
          (helm :sources (list
                          (helm-build-async-source "godoc"
                            :candidates-process
                            (symbol-function 'tka-godoc-search)
                            :action '(("Show GoDoc" . tka-show-GoDoc)))
                          (helm-build-sync-source "local"
                            :candidates (go-packages)
                            :action '(("Show GoDoc" . tka-show-GoDoc))))
                :buffer "*helm Go packages*"))

        (local-set-key (kbd "C-c C-p") 'tka-gopkg-list)
        )

      (add-hook 'go-mode-hook 'go-mode-setup)
      ;; auto-completion for go using company mode
      ;; set it up after go-mode-setup so that GOPATH is setup
      ;; TBD - not sure why moving the lambda inside go-mode-setup
      ;; doesn't work
      (add-hook 'go-mode-hook (lambda ()
                                (set (make-local-variable 'company-backends) '(company-go))
                                (company-mode)
                                (hs-minor-mode)
                                (yas-minor-mode)))
      )

  ;; manual install
 ;;(require 'ob-influxdb)

  (require 'ob-sql)

  ;; don't setup epdf tools on non-OSX systems
  ;;  (when (string-equal system-type "darwin")

                                                     ;;; brew tap dunn/emacs
                                                     ;;; Install epdfinfo via 'brew install pdf-tools' and then install the
                                                     ;;; pdf-tools elisp via the use-package below. To upgrade the epdfinfo
                                                     ;;; server, just do 'brew upgrade pdf-tools' prior to upgrading to newest
                                                     ;;; pdf-tools package using Emacs package system. If things get messed
                                                     ;;; up, just do 'brew uninstall pdf-tools', wipe out the elpa
                                                     ;;; pdf-tools package and reinstall both as at the start.
  ;;                                    (use-package pdf-tools
  ;;                                     :defer t
  ;;                                    :config
  ;;                                   (progn
  ;;                                    (setq pdf-tools-handle-upgrades nil) ; Use brew upgrade pdf-tools instead.
  ;;                                   (setq pdf-info-epdfinfo-program "/usr/local/bin/epdfinfo"))
  ;;                                  (pdf-tools-install)))

  ;;(require 'init-eshell)
  (use-package htmlize :defer t)

  ;; manual install
  ;; (require 'ob-go)

  (use-package interleave :defer t)

  ;; Org mode
  ;; Get org-headers to look pretty! E.g., * → ⊙, ** ↦ ◯, *** ↦ ★
  ;; https://github.com/emacsorphanage/org-bullets
  (use-package org-bullets :ensure t)
  (add-hook 'org-mode-hook 'org-bullets-mode)

  (progn
    (setq org-todo-keywords
          '((sequence "TODO" "IN-PROGRESS" "WAITING" "|" "DONE" "CANCELED")
            (sequence "LATER" "DONE")))
    ;; 1:urgent, 2:important, 3:later
    (setq org-priority-highest 1)
    (setq org-priority-lowest 3)
    (setq org-priorit-ddefault 3)
    (setq org-directory "~/Dropbox/orgdir")
    (setq org-agenda-files '("~/Dropbox/orgdir"))


    (setq org-capture-templates
          '(("p" "Personal task" entry (file "personal-tasks.org")
             "* TODO [#3] %? %^g\n
             %i"
             :empty-lines 1)
            ("o" "Office task" entry (file "office-tasks.org")
             "* TODO [#3] %? %^g\n
             %i"
             :empty-lines 1)
            ("l" "Later read/watch/think/do" entry (file "personal-later.org")
             "* LATER %? %^g\n
             %i"
             :empty-lines 1)))

    (setq org-agenda-custom-commands
      '(("p" "Personal agenda"
         ((agenda "")
          (tags-todo "CATEGORY=\"personal\""
           ((org-agenda-overriding-header "Personal tasks")
            (org-agenda-prefix-format "")
           ))))
        ("o" "Office agenda"
         ((agenda "")
          (tags-todo "CATEGORY=\"office\""
          ((org-agenda-overriding-header "Office tasks")
            (org-agenda-prefix-format ""))
      )))
      ("l" "Later agenda"
         ((tags-todo "CATEGORY=\"later\""
          ((org-agenda-overriding-header "Later tasks")
            (org-agenda-prefix-format ""))
      )))
))

    (setq org-log-done (quote time))
    (setq org-log-redeadline (quote time))
    (setq org-log-reschedule (quote time))

    (setq org-enforce-todo-dependencies t)

              ;;;;;; Fix Helm org tag completion
    ;; From Anders Johansson <https://groups.google.com/d/msg/emacs-helm/tA6cn6TUdRY/G1S3TIdzBwAJ>

    ;; This works great!  He posted it on 3 Mar 2016, on a thread that was
    ;; started in Oct 2013.  He also posted this message on 2 Apr 2014,
    ;; maybe an earlier attempt at a solution:
    ;; <http://article.gmane.org/gmane.emacs.orgmode/84495> I've just
    ;; tidied it up a bit and adjusted the prompt.

    (add-to-list 'helm-completing-read-handlers-alist '(org-capture . aj/org-completing-read-tags))
    (add-to-list 'helm-completing-read-handlers-alist '(org-set-tags . aj/org-completing-read-tags))

    (defun aj/org-completing-read-tags (prompt coll pred req initial hist def inh)
      (if (not (string= "Tags: " prompt))
          ;; Not a tags prompt.  Use normal completion by calling
          ;; `org-icompleting-read' again without this function in
          ;; `helm-completing-read-handlers-alist'
          (let ((helm-completing-read-handlers-alist (rassq-delete-all
                                                      'aj/org-completing-read-tags
                                                      helm-completing-read-handlers-alist)))
            (org-icompleting-read prompt coll pred req initial hist def inh))
        ;; Tags prompt
        (let* ((initial (and (stringp initial)
                             (not (string= initial ""))
                             initial))
               (curr (when initial
                       (org-split-string initial ":")))
               (table (org-uniquify
                       (mapcar 'car org-last-tags-completion-table)))
               (table (if curr
                          ;; Remove current tags from list
                          (cl-delete-if (lambda (x)
                                          (member x curr))
                                        table)
                        table))
               (prompt (if initial
                           (concat "Tags " initial)
                         prompt)))
          (concat initial (mapconcat 'identity
                                     (nreverse (aj/helm-completing-read-multiple
                                                prompt table pred nil nil hist def
                                                t "Org tags" "*Helm org tags*" ":"))
                                     ":")))))

    (defun aj/helm-completing-read-multiple (prompt choices
                                                    &optional predicate require-match initial-input hist def
                                                    inherit-input-method name buffer sentinel)
      "Read multiple items with `helm-completing-read-default-1'. Reading stops
              when the user enters SENTINEL. By default, SENTINEL is
              \"*done*\". SENTINEL is disambiguated with clashing completions
              by appending _ to SENTINEL until it becomes unique. So if there
              are multiple values that look like SENTINEL, the one with the
              most _ at the end is the actual sentinel value. See
              documentation for `ido-completing-read' for details on the
              other parameters."
      (let ((sentinel (or sentinel "*done*"))
            this-choice res done-reading)
        ;; Uniquify the SENTINEL value
        (while (cl-find sentinel choices)
          (setq sentinel (concat sentinel "_")))
        (setq choices (cons sentinel choices))
        ;; Read choices
        (while (not done-reading)
          (setq this-choice (helm-completing-read-default-1 prompt choices
                                                            predicate require-match initial-input hist def
                                                            inherit-input-method name buffer nil t))
          (if (equal this-choice sentinel)
              (setq done-reading t)
            (setq res (cons this-choice res))
            (setq prompt (concat prompt this-choice ":"))))
        res)))

  ;; (require 'init-org)
  ;; (use-package auto-package-update
  ;;      :defer t
  ;;      :config
  ;;      (setq auto-package-update-delete-old-versions t)
  ;;      (setq auto-package-update-hide-results t)
  ;;      (auto-package-update-maybe)
  ;;      (setq auto-package-update-prompt-before-update t))

  (use-package markdown-mode
    :defer t
    :commands (markdown-mode gfm-mode)
    :mode (("README\\.md\\'" . gfm-mode)
           ("\\.md\\'" . markdown-mode)
           ("\\.markdown\\'" . markdown-mode))
    :init (setq markdown-command "multimarkdown"))

  (use-package kubernetes
    :defer t
    :custom (kubernetes-logs-arguments (quote ("--tail=500")))
    :commands (kubernetes-overview))

  (use-package elfeed
    :defer t
    :bind
    ("C-x w" . elfeed)
    :config
    (setq elfeed-db-directory "~/dropbox/emacs/feeds_db/")
    (setq-default elfeed-search-filter "@1-week-ago +unread ")
    ;; Entries older than 2 weeks are marked as read
    (add-hook 'elfeed-new-entry-hook
              (elfeed-make-tagger :before "2 weeks ago"
                                  :remove 'unread)))

  ;; use Org mode to manage hierarchical list of feeds
  (use-package elfeed-org
    :defer t
    :config
    (elfeed-org)
    :config
    (setq rmh-elfeed-org-files (list "~/dropbox/emacs/feeds.org")))
  (use-package elfeed-goodies
    :defer t
    :config
    (elfeed-goodies/setup))


  (use-package fill-column-indicator :ensure t)
  (setq-default set-fill-column 85)
  (add-hook 'after-change-major-mode-hook 'fci-mode)
  (add-hook 'after-change-major-mode-hook 'auto-fill-mode)

  (use-package kapacitor :defer t)

  (use-package ace-window
    :ensure t
    :config
    (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
    :bind
    ("C-c w" . ace-window))


  (dolist (mode '(emacs-lisp-mode-hook
                  python-mode-hook
                  go-mode-hook))
    (add-hook mode
              '(lambda ()
                 (flyspell-prog-mode))))
  ;; active Babel languages
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((sql . t)))
  ;;or disable it for all blocks
  (setq org-confirm-babel-evaluate nil)
  ;; add additional languages with '((language . t)))


  ;;(setq influx-cli-arguments '("-host" "planck" "-precision" "rfc3339"))
  ;;(load-library "influx")

  ;; org-reveal setup
  (use-package ox-reveal
    :defer t
    :config
    (progn
      (setq org-reveal-root "file:////Users/kthommandra/reveal.js-3.8.0")
      ))

  ;; presentation mode which is minimalistic and useful when the slide deck
  ;; needs to be edited during presentation
  (use-package org-present
    :defer t
    :config
    (progn
      (add-hook 'org-present-mode-hook
                (lambda ()
                  (org-present-big)
                  (org-display-inline-images)
                  (org-present-hide-cursor)
                  (org-present-read-only)))
      (add-hook 'org-present-mode-quit-hook
                (lambda ()
                  (org-present-small)
                  (org-remove-inline-images)
                  (org-present-show-cursor)
                  (org-present-read-write)))
      ))

  (use-package paradox
    :defer t
    :config
    (paradox-enable))

  (use-package helpful :ensure t)
    (use-package esup :defer t)
  (use-package command-log-mode :defer t)
  (use-package aggressive-indent
    :ensure t
    :config
    (global-aggressive-indent-mode 1))

  (use-package ace-jump-mode
    :ensure t
    :config
    (define-key evil-normal-state-map (kbd "SPC") 'ace-jump-mode)
    )

  (use-package pyenv-mode :ensure t
    :config
    (pyenv-mode-set "3.8.0"))

  (use-package importmagic
    :ensure t
    :config
    (add-hook 'python-mode-hook 'importmagic-mode))

  (use-package elpy
    :ensure t
    :config
    (elpy-enable)
    (elpy-set-test-runner 'elpy-test-pytest-runner))

  (use-package blacken
    :ensure t
    :config
    (add-hook 'python-mode-hook 'blacken-mode))

  (use-package corral
    :ensure t
    :config
    (setq corral-preserve-point t)
    )

  ;; https://ericjmritz.wordpress.com/2015/10/14/some-personal-hydras-for-gnu-emacs/

  (use-package corral
    :ensure t
    :config
    (defhydra hydra-corral (:columns 4)
      "Corral"
      ("(" corral-parentheses-backward "Back")
      (")" corral-parentheses-forward "Forward")
      ("[" corral-brackets-backward "Back")
      ("]" corral-brackets-forward "Forward")
      ("{" corral-braces-backward "Back")
      ("}" corral-braces-forward "Forward")
      ("." hydra-repeat "Repeat"))
    (global-set-key (kbd "C-c c") #'hydra-corral/body))

  (outline-minor-mode 1)

  ;; https://github.com/abo-abo/hydra/wiki/Emacs
  (defhydra hydra-outline (:color pink :hint nil)
    "
        ^Hide^             ^Show^           ^Move
        ^^^^^^------------------------------------------------------
        _q_: sublevels     _a_: all         _u_: up
        _t_: body          _e_: entry       _n_: next visible
        _o_: other         _i_: children    _p_: previous visible
        _c_: entry         _k_: branches    _f_: forward same level
        _l_: leaves        _s_: subtree     _b_: backward same level
        _d_: subtree

        "
    ;; Hide
    ("q" hide-sublevels)    ; Hide everything but the top-level headings
    ("t" hide-body)         ; Hide everything but headings (all body lines)
    ("o" hide-other)        ; Hide other branches
    ("c" hide-entry)        ; Hide this entry's body
    ("l" hide-leaves)       ; Hide body lines in this entry and sub-entries
    ("d" hide-subtree)      ; Hide everything in this entry and sub-entries
    ;; Show
    ("a" show-all)          ; Show (expand) everything
    ("e" show-entry)        ; Show this heading's body
    ("i" show-children)     ; Show this heading's immediate child sub-headings
    ("k" show-branches)     ; Show all sub-headings under this heading
    ("s" show-subtree)      ; Show (expand) everything in this heading & below
    ;; Move
    ("u" outline-up-heading)                ; Up
    ("n" outline-next-visible-heading)      ; Next
    ("p" outline-previous-visible-heading)  ; Previous
    ("f" outline-forward-same-level)        ; Forward - same level
    ("b" outline-backward-same-level)       ; Backward - same level
    ("z" nil "leave"))

  ;;(global-set-key (kbd "C-c #") 'hydra-outline/body) ; by example

  ;; https://github.com/alphapapa/unpackaged.el#smerge-mode
  (use-package smerge-mode
    :ensure t
    :after hydra
    :config
    (defhydra unpackaged/smerge-hydra
      (:color pink :hint nil :post (smerge-auto-leave))
      "
        ^Move^       ^Keep^               ^Diff^                 ^Other^
        ^^-----------^^-------------------^^---------------------^^-------
        _n_ext       _b_ase               _<_: upper/base        _C_ombine
        _p_rev       _u_pper              _=_: upper/lower       _r_esolve
        ^^           _l_ower              _>_: base/lower        _k_ill current
        ^^           _a_ll                _R_efine
        ^^           _RET_: current       _E_diff
        "
      ("n" smerge-next)
      ("p" smerge-prev)
      ("b" smerge-keep-base)
      ("u" smerge-keep-upper)
      ("l" smerge-keep-lower)
      ("a" smerge-keep-all)
      ("RET" smerge-keep-current)
      ("\C-m" smerge-keep-current)
      ("<" smerge-diff-base-upper)
      ("=" smerge-diff-upper-lower)
      (">" smerge-diff-base-lower)
      ("R" smerge-refine)
      ("E" smerge-ediff)
      ("C" smerge-combine-with-next)
      ("r" smerge-resolve)
      ("k" smerge-kill-current)
      ("ZZ" (lambda ()
              (interactive)
              (save-buffer)
              (bury-buffer))
       "Save and bury buffer" :color blue)
      ("q" nil "cancel" :color blue))
    :hook (magit-diff-visit-file . (lambda ()
                                     (when smerge-mode
                                       (unpackaged/smerge-hydra/body)))))


  ;; HideShow mode
  ;; https://www.emacswiki.org/emacs/HideShow

  ;; requires 26.1
  ;;(use-package org-super-agenda
  ;;  :ensure t)

  (use-package speed-type :defer t)

  ;; Colour parens, and other delimiters, depending on their depth.
  ;; Very useful for parens heavy languages like Lisp.
  (use-package rainbow-delimiters :ensure t)

  (add-hook 'org-mode-hook
            '(lambda () (rainbow-delimiters-mode 1)))
  (add-hook 'prog-mode-hook
            '(lambda () (rainbow-delimiters-mode 1)))

  (use-package yasnippet-snippets :ensure t)

  (add-hook 'eshell-mode-hook
            (lambda ()
              (eshell-cmpl-initialize)
              (setq helm-show-completion-display-function #'helm-default-display-buffer)
              (setq helm-candidate-separator "........")
              (define-key eshell-mode-map [remap eshell-pcomplete] 'helm-esh-pcomplete)
              (define-key eshell-mode-map (kbd "M-p") 'helm-eshell-history)))

  (load (expand-file-name "~/quicklisp/slime-helper.el"))
  ;; Replace "sbcl" with the path to your implementation
  (setq inferior-lisp-program "sbcl")

#+END_SRC

* Post package
#+BEGIN_SRC emacs-lisp
  ;; Comment/un-comment
  (global-set-key (kbd "C-c ;") 'comment-or-uncomment-region)

  ;; Compile Command
  (global-set-key (kbd "C-x c c") 'compile)

  (define-key global-map (kbd "C-c a") 'org-agenda)
  (define-key global-map (kbd "C-c c") 'org-capture)

  (define-key flyspell-mode-map (kbd "C-;") 'helm-flyspell-correct)

  (evil-add-hjkl-bindings occur-mode-map 'emacs
    (kbd "/")       'evil-search-forward
    (kbd "n")       'evil-search-next
    (kbd "N")       'evil-search-previous
    (kbd "C-d")     'evil-scroll-down
    (kbd "C-u")     'evil-scroll-up
    (kbd "C-w C-w") 'other-window)

  (add-hook 'evil-insert-state-exit-hook (lambda () (evil-write nil nil)))
  (key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "jk" 'evil-write)

  (setq helm-time-zone-home-location "Seattle") ;; west coast

   ;; PDB Hook
    (defun my-shell-mode-hook ()
      (add-hook 'comint-output-filter-functions 'python-pdbtrack-comint-output-filter-function t))
    (add-hook 'shell-mode-hook 'my-shell-mode-hookq)

#+END_SRC
* Work related customizations
#+BEGIN_SRC emacs-lisp
;;(if (getenv "WP")
;;    (setq server-name (getenv "WP")))
;;(setq arastra-ignore-libs '( "arastra-indent" "nifty-buffer" "ap" "nifty-file" "ashell" "keyboard-macros" "arastra-utils" "misc-bindings" "a4-gid" "a4-gid2" "qt" "go-mode" ))
;;(load-library "Arastra")


#+END_SRC

* Credits
- [[https://github.com/jonathanchu/dotemacs/blob/master/init.el][Jonathan Chu]]
- [[https://github.com/magnars][magnars(@emacsrocks)]]
- https://github.com/zamansky/dot-emacs
