(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (flycheck-yamllint yaml-mode flycheck-gometalinter go-snippets helm-go-package go-projectile go-rename go-guru company-go go-eldoc go-mode markdown-mode+ markdown-mode yasnippet-snippets rainbow-delimiters corral blacken elpy importmagic pyenv-mode ace-jump-mode aggressive-indent helpful ace-window fill-column-indicator org-bullets evil-indent-textobject evil-surround evil-leader evil undo-tree helm-flycheck flycheck git-timemachine magit helm-swoop helm-ag helm-projectile helm-flyspell hydra imenu-list key-chord which-key doom-modeline auto-compile use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
